package sample;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class SimulatorMenuBar extends MenuBar {
    SimulatorMenuBar(Simulator simulator) {
        Menu controlMenu = new Menu("Control");
        Menu settingsMenu= new Menu("Settings");
        getMenus().addAll(controlMenu,settingsMenu);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        MenuItem speedup = new MenuItem("Speed up");
        MenuItem slowdown = new MenuItem("Slow down");
        controlMenu.getItems().addAll(start, stop);
        settingsMenu.getItems().addAll(speedup,slowdown);
        start.setOnAction(e -> {
            System.out.println("Start selected");
            simulator.start();
        });
        stop.setOnAction(e -> {
            System.out.println("Stop selected");
            simulator.stop();
        });
        start.setOnAction(e -> {
            System.out.println("Speed up selected");
            simulator.speedup();
        });
        stop.setOnAction(e -> {
            System.out.println("Slowdown selected");
            simulator.slowdown();
        });
    }
}
